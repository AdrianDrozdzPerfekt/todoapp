<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #wyswietlenie wszystkich notatek 
        //$todos = Todo::all();
        #wyswietlenie wszystkich notatek od najnowszych
        $todos = Todo::orderBy('created_at', 'desc')->get();
        #zwrocenie widoku i przekazanie
        #do niego danych pobranych z modelu
        #with('zmiennaWewWidoku', $zmienna)
        return view('todos.index')
                    ->with('todos', $todos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validacja formularza
        $this->validate($request, [
            'title' => 'required',
            'due' => 'required',
            'description' => 'required',
        ], [
            'title.required' => 'Proszę podać tytuł',
            'due.required' => 'Proszę podać datę wymagalności',
            'description.required' => 'Proszę podać opis',
        ]);

        try {
            //stworzenie notatki
            $todo = new Todo();
            $todo->title = $request->input('title');
            $todo->due = $request->input('due');
            $todo->description = $request->input('description');
            //proba zapisania notatki i przekierowanie na 
            // /todos lub na /todos/create w zaleznosci od tego
            // czy udalo sie zapisac notatke
            if ($todo->save()) {
                return redirect('/todos')->with('success','Dodano notatkę');
            } else {
                return redirect('/todos/create')
                    ->withInput()
                    ->with('error','Nie dodano notatki');
            }
        } catch (\Exception $e) {
            return redirect('/todos/create')
                    ->withInput()
                    ->with('error','Wystąpił błąd! Proszę spróbować ponownie.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            // znajdowanie notatki
            $todo = Todo::findOrFail($id);

            return view('todos.show')->with('todo', $todo);

        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie ma takiej notatki');
        } catch (\Exception $e) {
            return redirect('/todos')->with('error','Wystąpił błąd: ' . $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            // znajdowanie notatki
            $todo = Todo::findOrFail($id);

            return view('todos.edit')->with('todo', $todo);

        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie ma takiej notatki');
        } catch (\Exception $e) {
            return redirect('/todos')->with('error','Wystąpił błąd: ' . $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validacja formularza
        $this->validate($request, [
            'title' => 'required',
            'due' => 'required',
            'description' => 'required',
        ], [
            'title.required' => 'Proszę podać tytuł',
            'due.required' => 'Proszę podać datę wymagalności',
            'description.required' => 'Proszę podać opis',
        ]);

        try {
            //pobranie notatki i nadpisanie danych z formularza
            $todo = Todo::findOrFail($id);
            $todo->title = $request->input('title');
            $todo->due = $request->input('due');
            $todo->description = $request->input('description');
            //proba zapisania notatki i przekierowanie na 
            // /todos lub na /todos/create w zaleznosci od tego
            // czy udalo sie zapisac notatke
            if ($todo->save()) {
                return redirect('/todos')->with('success','Zapisano notatkę');
            } else {
                return redirect('/todos/'.$id.'/edit')
                    ->withInput()
                    ->with('error','Nie zapisano notatki');
            }
        } catch (ModelNotFoundException $e) {
            return redirect('/todos')
                    ->with('error','Nie znaleziono notatki.');
        } catch (\Exception $e) {
            return redirect('/todos')
                    ->with('error','Wystąpił błąd! Proszę spróbować ponownie.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // znajdowanie notatki
            $todo = Todo::findOrFail($id);

            if ($todo->delete()) {
                return redirect('/todos')->with('success','Usunięto notatkę');
            } else {
                return redirect('/todos')->with('error','Nie usunięto notatki');
            }

        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie ma takiej notatki');
        } catch (\Exception $e) {
            return redirect('/todos')->with('error','Wystąpił błąd: ' . $e->getMessage());
        }
    }
}
