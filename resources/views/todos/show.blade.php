@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<p><a href="/todos" class="btn btn-default">Powrót</a></p>
			<h1>{{$todo->title}}</h1>
			<div class="label label-danger">{{$todo->due}}</div>
			<hr>
			<p>{{$todo->description}}</p>
		</div>
		<div class="col-xs-12">
			<a class="btn btn-primary" href="/todos/{{$todo->id}}/edit" role="button">
				Edycja
			</a>
			{{ Form::open(['action' => ['TodosController@destroy', $todo->id], 'method' => 'POST', 'class' => 'pull-right'])}}
				{{ Form::hidden('_method', 'DELETE')}}
				{{ Form::submit('Usun', ['class' => 'btn btn-danger'])}}
			{{ Form::close() }}
		</div>
	</div>
@endsection