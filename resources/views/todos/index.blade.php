@extends('layouts.app')

@section('content')
	@if(count($todos))
		@foreach($todos as $todo)
			<div class="well">
				<h3>
					<a href="/todos/{{$todo->id}}">{{$todo->title}}</a>
					<span class="label label-danger pull-right">{{$todo->due}}</span>
				</h3>
			</div>
		@endforeach
	@else
		<div class="row">
			<div class="col-xs-12">
				<h1>Brak notatek</h1>
			</div>
		</div>
	@endif
@endsection