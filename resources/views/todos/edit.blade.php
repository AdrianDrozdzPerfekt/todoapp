@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			{{-- FORMULARZ --}}
			<h1>Edycja Notatki</h1>
			{{ Form::open(['action' => ['TodosController@update', $todo->id], 'method' => 'POST']) }}
				{{Form::hidden('_method', 'PUT')}}
				<div class="form-group">
					{{ Form::label('title', 'Tytuł', ['class' => 'control-label']) }}
					{{ Form::text('title', $todo->title,['class' => 'form-control', 'placeholder' => 'np. Zakupy z Biedronce'])}}
				</div>
				<div class="form-group">
					{{ Form::label('due', 'Data wymagalności', ['class' => 'control-label']) }}
					{{ Form::text('due', $todo->due,['class' => 'form-control', 'placeholder' => 'np. Poniedziałek wieczór'])}}
				</div>
				<div class="form-group">
					{{ Form::label('description', 'Opis zadania', ['class' => 'control-label']) }}
					{{ Form::textarea('description', $todo->description,['class' => 'form-control'])}}
				</div>
				<div class="form-group text-right">
					{{ Form::submit('Zapisz notatkę', ['class' => 'btn btn-success'])}}
				</div>
			{{ Form::close() }}
		</div>
	</div>
@endsection