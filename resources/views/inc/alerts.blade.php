<div class="row">
	<div class="col-xs-12" id="alert-messages">
		<!-- wyswietlanie bledow formularza -->
		@if (count($errors) > 0)
			@foreach ($errors as $error)
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ $error }}
				</div>
			@endforeach
		@endif

		<!-- error message do pokazywania bledow zapisow do bazy danych -->
		@if (session('error'))
			<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ session('error') }}
				</div>
		@endif

		<!-- success message do pokazywania udanego zapisu do bazy danych -->
		@if (session('success'))
			<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ session('success') }}
				</div>
		@endif
	</div>
</div>
