
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Starter Template for Bootstrap</title>
    <link rel="stylesheet" href="/css/app.css">    
  </head>

  <body>
    @include('inc.navbar')
    <div class="container">
      @include('inc.alerts')
      @yield('content')
      <footer id="footer" class="text-center">
        <p>Copyright &copy; 2017 TodoApp</p>
      </footer>
    </div><!-- /.container -->
    <script src="/js/app.js"></script>
  </body>
</html>
